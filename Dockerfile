FROM php:7.3-apache

RUN apt-get update && apt-get install -y libxml2-dev
RUN docker-php-ext-install opcache pdo pdo_mysql xml

COPY . /var/www/html
WORKDIR /var/www/html

RUN chown -R www-data:www-data var/cache
RUN chown -R www-data:www-data var/log

COPY apache.conf /etc/apache2/sites-enabled/000-default.conf