<?php
/**
 * Created by PhpStorm.
 * User: prauc
 * Date: 16.03.19
 * Time: 17:17
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class LuckyController
{
    public function number()
    {
        $number = random_int(0, 11);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }
}